const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session')


// Enable request Cookies
app.use(cookieParser());

// Enable Server Sessions
app.use(session({
  secret: 'a20204o03503',
  resave: false,
  saveUninitialized:false,
  cookie:{
    secure:false, //allows session to work on HTTP
    httpOnly:true, //Disable this cookie for document.cookie
    maxAge: 3600000
  }
}));

//Serve my static content(js, css, html)
app.use('/public', express.static(path.join(__dirname, 'www')));

app.get('/',(req, res) => {
    if (req.session.counter == undefined) {
      res.send('No session exists for this user...');
    } else {
      req.session.counter +=1;
      res.send('Number of site visits for this user: ' + req.session.counter);
    }
   
});

app.get('/create',(req,res)=>{
  req.session.counter = 0;
  res.send('session was created')
});

app.listen(3000, () => console.log('app started on port 3000'));
    
    


  

